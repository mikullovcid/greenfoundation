import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:permission_handler/permission_handler.dart';


class Maps extends StatefulWidget {
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Maps> {
final geo = Geolocator();
  MapController mapController = MapController();
  LatLng latLng;
  StreamSubscription<Position> positionStream;
  bool interacting = false;
  List<Marker> markers;

  var shit = [];

  getPos() async {
    await Permission.location.request();
        if (await Permission.location.isGranted &&
        await geo.isLocationServiceEnabled()) {
      var pos =
          await geo.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
          // print(pos);
      // List<Placemark> country = await geo.placemarkFromCoordinates(pos.latitude, pos.longitude);
      // var coords = [country[0].position.latitude.toString(), country[0].position.longitude.toString()];
        
        // LatLng useNow = latLng;
      // print(latLng);
      await Future.delayed(Duration(milliseconds: 1000), ()async{
      
      setState(() {
        latLng = LatLng(
      42.657, 21.147
      // pos.latitude, pos.longitude
        );
      markers = [
          new Marker(
            width: 80.0,
            height: 80.0,
            point:  latLng,
            builder: (ctx) =>
            new Container(
              child: const Icon(Icons.assistant_navigation),
            ),
          ),
        ];
      });
        mapController.onReady.then((_) => mapController.move(latLng, 16));
      
        
      });
    } else {
      return [42.0, 40.0];
    }
  }

  movementListener() async {
  positionStream = geo.getPositionStream().listen(
    (Position position) {
        if(position != null){
          setState(() {
            latLng = LatLng(position.latitude, position.longitude);
          });
        } 
    });
  }

  @override
  void dispose() {
    positionStream != null ? positionStream.cancel() : (){};
    super.dispose();
  }

  moveTo(pos)async{
    mapController.move(pos.point, 16.0);
    Navigator.pop(context);
  }


  @override
  void initState() {
    getPos();
    // movementListener();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
  return latLng != null && markers != null ? 
  
  Scaffold(
    appBar: AppBar(
      bottomOpacity: 1.0,
      toolbarOpacity: 1.0,
      backgroundColor: Colors.green
    ),
    drawer: Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: 
          shit.map((e) => GestureDetector(
            onTap: (){
              Navigator.pop(context);
              showModalBottomSheet(context: context, builder: (BuildContext context) {
              return Container(
                height: 200,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                       Text(e['name'].toString()),
                      ElevatedButton(
                        child: const Text('Close'),
                        onPressed: () => Navigator.pop(context),
                      ),
                      ElevatedButton(
                        child: const Text('Go to Mark'),
                        onPressed: () {
                          // Navigator.pop(context);
                        moveTo(e['mark']);
                        },
                      )
                    ],
                  ),
                ),
              );
            },
              );
            },
            child: Container(margin: EdgeInsets.all(10), child:Text('${e['name']}')))).toList()
        ,
      ),
    ),
      body: GestureDetector(
        // onPanStart: (_){
        //   !interacting ?
        //   setState(() {
        //     interacting = true;
        //   }) : (){};
        // },
        // onPanCancel: (){
        //   interacting ? 
        //   setState((){
        //     interacting = false;
        //   }) : (){};
        // },
        // onPanEnd: (_){
        //   interacting ? 
        //   setState((){
        //     interacting = false;
        //   }) : (){};
        // },
        // onVerticalDragStart: (_){
        //     !interacting ?
        //   setState(() {
        //     interacting = true;
        //   }) : (){};
        // },
        // onVerticalDragEnd: (_){
        //   interacting ? 
        //   setState((){
        //     interacting = false;
        //   }) : (){};
        // },
        // onHorizontalDragEnd: (_){
        //   interacting ? 
        //   setState((){
        //     interacting = false;
        //   }) : (){};
        // },
        // onHorizontalDragStart: (_){
        //     !interacting ?
        //   setState(() {
        //     interacting = true;
        //   }) : (){};
        // },
        
              child:  FlutterMap(
        mapController: mapController,
        options:  MapOptions(
          interactive: true,
          onLongPress: (pos){
            if(!interacting){
              var newMark =
                 Marker(
                width: 80.0,
                height: 80.0,
                point:  pos,
                builder: (ctx) =>
                GestureDetector(
                  onTap: (){
                    showGeneralDialog(context: context, 
                    barrierDismissible: false,
                    transitionBuilder: (context, a1, a2, widget){
                      final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
                      return Transform(transform: Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
                      child: Opacity(
                        opacity: a1.value,
                                          child: Scaffold(
                                    body: Container(
                        height: 200,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                               Text('position ${pos.latitude} , ${pos.longitude}'),
                              ElevatedButton(
                                child: const Text('Close'),
                                onPressed: () => Navigator.pop(context),
                              )
                            ],
                          ),
                        ),
                    ),
                  ),
                      ),
                      );
                    },
                    transitionDuration: Duration(milliseconds: 500),
                    pageBuilder: (context, animation1, animation2) {          
                },
                 );
                  },
                              child:  Container(
                    child: const Icon(Icons.location_on),
                  ),
                ),
              
              );
            markers.add(newMark);
            shit.add({
              'name': 'edit name',
              'mark' : newMark
            });
            setState(() {
            });
            }
          },
          center: latLng,
          // zoom: 16.0,
          // maxZoom: 18.4,
          // minZoom: 3.0
        ),
        layers: [
           TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']
          ),
          new MarkerLayerOptions(
            markers: markers,
          ),
        ],
    ),
      ),
  ) : Center(child: CircularProgressIndicator(),) ;
}
}