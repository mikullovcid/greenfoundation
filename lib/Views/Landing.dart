import 'package:flutter/material.dart';
import 'package:green_foundation/Views/Map.dart';

class Landing extends StatefulWidget {
  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        appBar: AppBar(
          bottomOpacity: 1.0,
          toolbarOpacity: 1.0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.green),
        ),
        drawerScrimColor: Colors.green,
        backgroundColor: Colors.green,
        drawer: Drawer(
            elevation: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, 
                    MaterialPageRoute(builder: (context) => new Maps()));
                  },
                  child: Text('Harta', style: TextStyle(color:Colors.lightGreen))),
                Text('2', style: TextStyle(color:Colors.lightGreen)),
                Text('3', style: TextStyle(color:Colors.lightGreen)),
                Text('4', style: TextStyle(color:Colors.lightGreen)),
                Text('5', style: TextStyle(color:Colors.lightGreen)),
              ],
            ),
          ),
        body: Container(
          child: 
          Center(child: Text('BODY')),
          ),
      ),
    );
  }
}