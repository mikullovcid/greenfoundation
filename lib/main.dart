import 'dart:async';

import 'package:cupertino_back_gesture/cupertino_back_gesture.dart';
import 'package:flutter/material.dart';
import 'package:green_foundation/Views/Landing.dart';
// import 'package:green_foundation/Views/Map.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

// Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   await Firebase.initializeApp();
//   print(message);
// }

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  initializeFlutterFire() async {
    try{
      await Firebase.initializeApp();
      FirebaseMessaging messaging = FirebaseMessaging.instance;
      messaging.setForegroundNotificationPresentationOptions();
    }catch(e){
      print(e);
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Green Foundation',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

////////////////

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);



  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // int _counter = 0;

  // void _incrementCounter() {
  //   setState(() {
  //     _counter++;
  //   });
  // }
  @override
  Widget build(BuildContext context) {
    return BackGestureWidthTheme(
          backGestureWidth: BackGestureWidth.fraction(1 / 2), 
          child:
            MaterialApp(
              theme: ThemeData(
                pageTransitionsTheme: PageTransitionsTheme(
                  builders: {
                    TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
                    TargetPlatform.iOS:
                        CupertinoPageTransitionsBuilderCustomBackGestureWidth(),
                  },
                ),
                scaffoldBackgroundColor: const Color.fromRGBO(29, 34, 62, 1),
                textTheme: TextTheme(
                  headline1: TextStyle(
                    fontSize: 22.0,
                    color: Color.fromRGBO(225, 33, 96, 1),
                    fontWeight: FontWeight.bold,
                    fontFamily: 'OpenSans',
                  ),
                  headline2: TextStyle(
                    fontSize: 28.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'OpenSans',
                  ),
                  bodyText1: TextStyle(
                    fontSize: 14.0,
                    fontFamily: 'OpenSans',
                    color: Colors.white,
                  ),
                  bodyText2: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'OpenSans',
                      color: Colors.white70),
                ),
              ),
              navigatorKey: navigatorKey,
              debugShowCheckedModeBanner: false,
              title: 'Green Foundation',
              home: Landing()
            ),
          
        );
  }
  
}
